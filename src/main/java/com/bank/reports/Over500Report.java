package com.bank.reports;

import com.bank.accounts.Account;

import java.math.BigDecimal;

public class Over500Report implements Report {

    @Override
    public Account visit(Account account) {
        if (account.getAmount().compareTo(new BigDecimal(500)) > 0) {
            return account;
        }

        return null;
    }
}
