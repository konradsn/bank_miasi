package com.bank.reports;

import com.bank.accounts.Account;

public interface Report {
    Account visit(Account account);
}
