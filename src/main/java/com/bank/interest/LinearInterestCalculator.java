package com.bank.interest;

import java.math.BigDecimal;

public class LinearInterestCalculator extends InterestCalculator{
    public LinearInterestCalculator() {
        super(0);
    }

    public LinearInterestCalculator(float rate) {
        super(rate);
    }

    public BigDecimal getAmountWithInterest(BigDecimal amount){
        BigDecimal result = super.getAmountWithInterest(amount);

        return result.add(result.multiply(BigDecimal.valueOf(rate)));
    }
}
