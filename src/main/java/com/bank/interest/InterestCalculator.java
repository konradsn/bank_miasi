package com.bank.interest;

import java.math.BigDecimal;

public abstract class InterestCalculator {
    protected float rate;

    public InterestCalculator(float rate) {
        this.rate = rate;
    }

    public BigDecimal getAmountWithInterest(BigDecimal amount){
        return amount;
    }
}
