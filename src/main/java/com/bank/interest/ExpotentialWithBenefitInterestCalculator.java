package com.bank.interest;

import java.math.BigDecimal;

public class ExpotentialWithBenefitInterestCalculator extends InterestCalculator{

    public ExpotentialWithBenefitInterestCalculator(float rate) {
        super(rate);
    }

    @Override
    public BigDecimal getAmountWithInterest(BigDecimal amount) {
        BigDecimal result = super.getAmountWithInterest(amount);

        return result.add(result.multiply(BigDecimal.valueOf(rate)).multiply(BigDecimal.valueOf(rate))).add(new BigDecimal(1000));
    }
}
