package com.bank.operations;

import com.bank.accounts.Account;

public interface AccountOperation {
    void execute(Account account) throws Exception;
}
