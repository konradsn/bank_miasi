package com.bank.operations;

import com.bank.accounts.Account;

import java.math.BigDecimal;

public class Deposit implements AccountOperation {
    private BigDecimal depositValue;

    public Deposit(BigDecimal depositValue) {
        this.depositValue = depositValue;
    }

    @Override
    public void execute(Account account) {
        account.addAmount(this.depositValue);
    }
}
