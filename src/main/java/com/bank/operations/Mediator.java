package com.bank.operations;

import com.bank.accounts.Account;
import com.bank.client.Bank;
import com.bank.client.Client;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class Mediator {

    private List<Bank> bankList;

    public Mediator() {
        bankList = new ArrayList<>();
    }

    public void addBankToCIR(Bank bank) {
        bankList.add(bank);
    }

    private Bank getBankWithGivenId(String bankId) {
        return bankList.stream().filter(bank -> bank.getBankId().equals(bankId)).collect(Collectors.toList()).get(0);
    }


    private Account getAccountFromAccountNumber(Bank bank, String accountNumber) {
        return bank.getClients().stream()
                .map(client -> client.getAccounts().stream().filter(account -> account.getAccountNumber().equals(accountNumber)).findFirst().get())
                .collect(Collectors.toList()).get(0);
    }

    private boolean checkIfDestinationAccountNumberExist(String transferDestination) {
        boolean accountNumberHasCorrectId = bankList.stream()
                .anyMatch(bank -> bank.getBankId().equals(transferDestination.substring(0, 2)));
        boolean accountNumberExistInBank = bankList.stream()
                .anyMatch(bank -> {
                    List<Client> clientList = bank.getClients();
                    return clientList.stream().anyMatch(client -> client.getAccounts().stream().anyMatch(account -> account.getAccountNumber().equals(transferDestination)));
                });
        return accountNumberExistInBank && accountNumberHasCorrectId;
    }

    public void makeExternalBankTransfer(Account transferSource, String transferDestination, BigDecimal amount) throws Exception {
        String destinationBankId = transferDestination.substring(0, 2);
        if (checkIfDestinationAccountNumberExist(transferDestination)) {
            Bank bankWithDestinationAccount = getBankWithGivenId(destinationBankId);
            Account accountTransferDestination = getAccountFromAccountNumber(bankWithDestinationAccount, transferDestination);

            accountTransferDestination.addAmount(amount);
            transferSource.subtractAmount(amount);
        } else {
            throw new Exception("Destination account doesn't exist!");
        }
    }

    public void makeInternalBankTransfer(Account transferSource, String transferDestination, BigDecimal amount, Bank clientBank) throws Exception{
        if (checkIfDestinationAccountNumberExist(transferDestination)) {
            Account accountTransferDestination = getAccountFromAccountNumber(clientBank, transferDestination);
            accountTransferDestination.addAmount(amount);
            transferSource.subtractAmount(amount);
        } else {
            throw new Exception("Destination account doesn't exist!");
        }
    }
}
