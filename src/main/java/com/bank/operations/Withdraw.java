package com.bank.operations;

import com.bank.accounts.Account;

import java.math.BigDecimal;

public class Withdraw implements AccountOperation{

    private BigDecimal withdrawValue;

    public Withdraw(BigDecimal withdrawValue) {
        this.withdrawValue = withdrawValue;
    }

    @Override
    public void execute(Account account) {
        account.subtractAmount(this.withdrawValue);
    }
}
