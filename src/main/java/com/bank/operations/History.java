package com.bank.operations;

import com.bank.operations.AccountOperation;

import java.util.ArrayList;
import java.util.List;

public class History {
    List<AccountOperation> accountOperationHistoryList = new ArrayList<>();

    public List<AccountOperation> getAccountOperationHistoryList() {
        return accountOperationHistoryList;
    }

    public void logOperation(AccountOperation accountOperation) {
        accountOperationHistoryList.add(accountOperation);
    }

    public void displayOperationHistory(){
        System.out.println("========= HISTORY =========");
        accountOperationHistoryList.forEach(System.out::println);
    }
}
