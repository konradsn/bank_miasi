package com.bank.operations;

import com.bank.accounts.Account;
import com.bank.client.Bank;

import java.math.BigDecimal;

public class BankTransfer implements AccountOperation {
    private Mediator mediator;
    private Bank clientBank;
    private BigDecimal amount;
    private String transferDestination;


    public BankTransfer(
            Bank clientBank,
            BigDecimal amount,
            String transferDestination,
            Mediator mediator) {
        this.clientBank = clientBank;
        this.amount = amount;
        this.transferDestination = transferDestination;
        this.mediator = mediator;
    }

    @Override
    public void execute(Account transferSource) throws Exception {
        if (checkIfTransferIsInternal()){
            mediator.makeInternalBankTransfer(transferSource, transferDestination, amount, clientBank);
        } else {
            mediator.makeExternalBankTransfer(transferSource, transferDestination, amount);
        }
    }

    private boolean checkIfTransferIsInternal(){
        return clientBank.getBankId().equals(transferDestination.substring(0,2));
    }
}
