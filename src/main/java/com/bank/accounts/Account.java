package com.bank.accounts;

import com.bank.listeners.AccountOperationListener;
import com.bank.operations.History;
import com.bank.operations.AccountOperation;
import com.bank.interest.InterestCalculator;
import com.bank.reports.Report;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class Account {
    private BigDecimal amount;
    private InterestCalculator interestCalculator;
    private History accountHistory;
    private String accountNumber;
    private List<AccountOperationListener> accountOperationListeners = new ArrayList<>();

    public String getAccountNumber() {
        return accountNumber;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public History getAccountHistory() {
        return accountHistory;
    }

    public InterestCalculator getInterestCalculator() {
        return interestCalculator;
    }

    public void addAmount(BigDecimal amount) {
        this.amount = this.amount.add(amount);
    }

    public void subtractAmount(BigDecimal amount) {
        this.amount = this.amount.subtract(amount);
    }

    public Account(InterestCalculator interestCalculator, String bankId) {
        amount = new BigDecimal(0);
        accountHistory = new History();
        this.accountNumber = bankId + java.util.UUID.randomUUID().toString().substring(0,8);

        this.interestCalculator = interestCalculator;
    }

    public void addAccountOperationListener(AccountOperationListener accountOperationListener){
        accountOperationListeners.add(accountOperationListener);
    }

    public void recalculateInterest() {
        amount = interestCalculator.getAmountWithInterest(amount);
    }

    public void makeOperation(AccountOperation accountOperation) throws Exception {
        accountOperation.execute(this);

        accountOperationListeners.forEach(accountOperationListener -> accountOperationListener.update(this, accountOperation));
    }

    public Account accept(Report report){
        return report.visit(this);
    }
}
