package com.bank.accounts;
import com.bank.interest.InterestCalculator;
import com.bank.listeners.AccountOperationListener;
import com.bank.operations.AccountOperation;
import com.bank.operations.History;
import com.bank.reports.Report;

import java.math.BigDecimal;

public class Investment extends Account{
    private Account account;
    private BigDecimal investmentAmount;

    public BigDecimal getInvestmentAmount() {
        return investmentAmount;
    }

    public Investment(Account account, String bankId) {
        super(account.getInterestCalculator(), bankId);
        this.account = account;
    }

    @Override
    public void recalculateInterest() {
        investmentAmount = account.getInterestCalculator().getAmountWithInterest(investmentAmount);
    }

    @Override
    public Account accept(Report report) {
        return account.accept(report);
    }

    public void closeToEarly(){
        account.addAmount(investmentAmount);
    }

    public void closeOK(){
        recalculateInterest();
        account.addAmount(investmentAmount);
    }

    @Override
    public String getAccountNumber() {
        return account.getAccountNumber();
    }

    @Override
    public BigDecimal getAmount() {
        return account.getAmount();
    }

    @Override
    public History getAccountHistory() {
        return account.getAccountHistory();
    }

    @Override
    public InterestCalculator getInterestCalculator() {
        return account.getInterestCalculator();
    }

    @Override
    public void addAmount(BigDecimal amount) {
        account.subtractAmount(amount);
        this.investmentAmount = amount;
    }

    @Override
    public void subtractAmount(BigDecimal amount) {
        account.subtractAmount(amount);
    }

    @Override
    public void addAccountOperationListener(AccountOperationListener accountOperationListener) {
        account.addAccountOperationListener(accountOperationListener);
    }
}
