package com.bank.accounts;

import com.bank.interest.InterestCalculator;
import com.bank.listeners.AccountOperationListener;
import com.bank.operations.History;
import com.bank.reports.Report;

import java.math.BigDecimal;

public class Credit extends Account {
    private Account account;
    private BigDecimal creditValueToReturn;

    public BigDecimal getCreditValueToReturn() {
        return creditValueToReturn;
    }

    public Credit(Account account, String bankId) {
        super(account.getInterestCalculator(), bankId);

        this.creditValueToReturn = new BigDecimal(0);
        this.account = account;
    }

    @Override
    public String getAccountNumber() {
        return account.getAccountNumber();
    }

    @Override
    public BigDecimal getAmount() {
        return account.getAmount();
    }

    @Override
    public History getAccountHistory() {
        return account.getAccountHistory();
    }

    @Override
    public InterestCalculator getInterestCalculator() {
        return account.getInterestCalculator();
    }

    @Override
    public void addAmount(BigDecimal amount) {
        account.addAmount(amount);
        creditValueToReturn = creditValueToReturn.add(account.getInterestCalculator().getAmountWithInterest(amount));
    }

    @Override
    public void subtractAmount(BigDecimal amount) {
        creditValueToReturn = creditValueToReturn.subtract(amount);
    }

    @Override
    public void addAccountOperationListener(AccountOperationListener accountOperationListener) {
        account.addAccountOperationListener(accountOperationListener);
    }

    @Override
    public void recalculateInterest() {
        account.recalculateInterest();
    }

    @Override
    public Account accept(Report report) {
        return account.accept(report);
    }
}
