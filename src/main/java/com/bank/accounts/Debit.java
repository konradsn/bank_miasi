package com.bank.accounts;

import com.bank.interest.InterestCalculator;
import com.bank.operations.AccountOperation;
import com.bank.reports.Report;

import java.math.BigDecimal;

public class Debit extends Account {
    private Account account;

    private BigDecimal debit;
    private BigDecimal limit;

    public BigDecimal getDebit() {
        return debit;
    }

    public Debit(Account account, String bankId,  BigDecimal limit) {
        super(account.getInterestCalculator(), bankId);

        this.debit = new BigDecimal(0);
        this.account = account;
        this.limit = limit;
    }

    @Override
    public BigDecimal getAmount() {
        return account.getAmount();
    }

    @Override
    public InterestCalculator getInterestCalculator() {
        return account.getInterestCalculator();
    }

    @Override
    public void recalculateInterest() {
        account.recalculateInterest();
    }

    @Override
    public Account accept(Report report) {
        return account.accept(report);
    }

    @Override
    public void addAmount(BigDecimal amount) {
        BigDecimal newAmount = new BigDecimal(0);

        if (debit.compareTo(new BigDecimal(0)) == 1) {
            debit = debit.subtract(amount);

            if (debit.compareTo(new BigDecimal(0)) == -1) {
                newAmount = account.getAmount().add(debit.abs());
                debit = new BigDecimal(0);
            }
        } else {
            newAmount = amount;
        }

        account.addAmount(newAmount);
    }

    @Override
    public void subtractAmount(BigDecimal amount){
        BigDecimal newAmount;

        if(debit.compareTo(new BigDecimal(0)) == 1){
            debit = debit.add(amount);
            if(debit.compareTo(limit) == 1){
                debit = debit.subtract(amount);
            }
        }else{
            account.subtractAmount(amount);
            if(account.getAmount().compareTo(new BigDecimal(0)) == -1){
                newAmount = account.getAmount().abs();
                debit = debit.add(newAmount);
                if(debit.compareTo(limit) == 1){
                    debit = debit.subtract(newAmount);
                    account.addAmount(amount);
                }else {
                    account.addAmount(newAmount);
                }

            }
        }
    }
}
