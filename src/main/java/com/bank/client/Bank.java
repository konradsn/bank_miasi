package com.bank.client;

import com.bank.accounts.Account;
import com.bank.interest.InterestCalculator;
import com.bank.reports.Report;

import java.util.ArrayList;
import java.util.List;

public class Bank {
    private String name;
    private String address;
    private List<Client> clients = new ArrayList<>();

    private String bankId;

    public String getName() {
        return name;
    }

    public String getAddress() {
        return address;
    }

    public List<Client> getClients() {
        return clients;
    }

    public String getBankId() {
        return bankId;
    }

    public void setBankId(String bankId) {
        this.bankId = bankId;
    }


    public Bank(String name, String address, String bankId){
        this.name = name;
        this.address = address;
        this.bankId = bankId;
    }

    public Client createClient(ClientInformation clientInformation, InterestCalculator interestCalculator) {
        Account account = new Account(interestCalculator, bankId);
        Client client = new Client(clientInformation, account, this);

        clients.add(client);

        return client;
    }

    public List<Account> makeReport(Report report) {
        List<Account> result = new ArrayList<>();

        clients.forEach(client -> client.getAccounts()
                .forEach(account -> {
                    Account visitedAccount = account.accept(report);

                    if(visitedAccount != null){
                        result.add(visitedAccount);
                    }
                }));

        return result;
    }
}
