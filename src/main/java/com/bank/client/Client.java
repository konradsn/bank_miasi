package com.bank.client;

import com.bank.accounts.Account;
import com.bank.accounts.Credit;
import com.bank.accounts.Investment;
import com.bank.operations.*;
import com.bank.interest.InterestCalculator;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class Client {
    private ClientInformation clientInformation;
    private String identifier;
    private List<Account> accounts = new ArrayList<>();
    private Bank clientBank;

    public ClientInformation getClientInformation() {
        return clientInformation;
    }

    public String getIdentifier() {
        return identifier;
    }

    public List<Account> getAccounts() {
        return accounts;
    }

    public Bank getClientBank() {
        return clientBank;
    }

    public Client(ClientInformation clientInformation, Account clientAccount, Bank clientBank) {
        this.clientInformation = clientInformation;
        this.identifier = java.util.UUID.randomUUID().toString();
        this.clientBank = clientBank;

        this.accounts.add(clientAccount);
    }

    public void makeDeposit(Account account, BigDecimal amount) {
        AccountOperation deposit = new Deposit(amount);
        try {
            account.makeOperation(deposit);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void makeWithdraw(Account account, BigDecimal amount) {
        AccountOperation withdraw = new Withdraw(amount);
        try {
            account.makeOperation(withdraw);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void makeAmountTransfer(Account from, String accountTo, BigDecimal amount, Mediator mediator) throws Exception {
        AccountOperation bankTransfer = new BankTransfer(clientBank, amount, accountTo, mediator); //klienta nie powinno obchodzić czy przelew jest internal czy external
        from.makeOperation(bankTransfer);
    }

    public Account createNewAccount(InterestCalculator interestCalculator) {
        Account account = new Account(interestCalculator, clientBank.getBankId());
        accounts.add(account);

        return account;
    }


    public void closeInvestmentBeforeTime(Investment investmentToClose){
        investmentToClose.closeToEarly();
    }

    public void closeInvestmentOnTime(Investment investmentToClose){
        investmentToClose.closeOK();
    }

    public void addAccount(Account account){
        accounts.add(account);
    }


}
