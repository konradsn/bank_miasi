package com.bank.client;

public class ClientInformation {
    private String name;
    private String nik;

    public String getName() {
        return name;
    }

    public String getNik() {
        return nik;
    }

    public ClientInformation(String name, String nik) {
        this.name = name;
        this.nik = nik;
    }
}
