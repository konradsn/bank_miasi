package com.bank.listeners;

import com.bank.accounts.Account;
import com.bank.operations.AccountOperation;

public  abstract class  AccountOperationListener {
    public void update(Account sender, AccountOperation accountOperation)
    {
    }
}
