package com.bank.listeners;

import com.bank.accounts.Account;
import com.bank.operations.AccountOperation;

public class AccountOperationHistoryListener extends  AccountOperationListener {
    @Override
    public void update(Account account, AccountOperation accountOperation) {
        account.getAccountHistory().logOperation(accountOperation);
        account.getAccountHistory().displayOperationHistory();
    }
}
