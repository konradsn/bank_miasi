import com.bank.accounts.Account;
import com.bank.accounts.Credit;
import com.bank.accounts.Investment;
import com.bank.client.Bank;
import com.bank.client.Client;
import com.bank.client.ClientInformation;
import com.bank.interest.LinearInterestCalculator;
import com.bank.operations.Mediator;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class CreditTests {

    private List<Client> users = new ArrayList<>();
    private List<Bank> banks = new ArrayList<>();

    @Before
    public void setUp() {
        Bank bank = new Bank("MYBANK", "ul. Bankowa 1, Poznań", generateUniqueBankId());

        banks.add(bank);

        ClientInformation clientInformation = new ClientInformation("Filip Philavong", "111111111");

        users.add(bank.createClient(clientInformation, new LinearInterestCalculator(0.2f)));
    }

    private String generateUniqueBankId() {
        String id = java.util.UUID.randomUUID().toString().substring(0, 2);
        if (banks.stream().anyMatch(bank -> bank.getBankId().equals(id))) {
            generateUniqueBankId();
        }
        return id;
    }

    @Test
    public void takeCredit() {
        //Given
        Client user = users.get(0);
        Account userAccount = user.getAccounts().get(0);
        user.makeDeposit(userAccount, new BigDecimal(1000));

        BigDecimal initialAmount = userAccount.getAmount();
        Credit userCredit = new Credit(userAccount, user.getClientBank().getBankId());
        user.addAccount(userCredit);

        //When
        user.makeDeposit(userCredit, new BigDecimal(1500));

        //Then
        BigDecimal truncatedAmount= userCredit.getCreditValueToReturn().setScale(0,BigDecimal.ROUND_DOWN);
        Assert.assertEquals(initialAmount.add(new BigDecimal(1500)), userAccount.getAmount());
        Assert.assertEquals(truncatedAmount, new BigDecimal(1800));
    }

    @Test
    public void payOffCredit() {
        //Given
        Client user = users.get(0);
        Account userAccount = user.getAccounts().get(0);
        user.makeDeposit(userAccount, new BigDecimal(1000));

        BigDecimal initialAmount = userAccount.getAmount();
        Credit userCredit = new Credit(userAccount, user.getClientBank().getBankId());
        user.addAccount(userCredit);

        //When
        user.makeDeposit(userCredit, new BigDecimal(1500));
        user.makeWithdraw(userCredit, new BigDecimal(1000));

        //Then
        BigDecimal truncatedAmount = userCredit.getCreditValueToReturn().setScale(0,BigDecimal.ROUND_DOWN);
        Assert.assertEquals(initialAmount.add(new BigDecimal(1500)), userAccount.getAmount());
        Assert.assertEquals(truncatedAmount, new BigDecimal(800));
    }
}
