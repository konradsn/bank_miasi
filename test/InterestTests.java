import com.bank.client.Bank;
import com.bank.client.Client;
import com.bank.client.ClientInformation;
import com.bank.interest.LinearInterestCalculator;

public class InterestTests {
    private Client user;

    public void setUp() {
        Bank bank = new Bank("MYBANK", "ul. Bankowa 1, Poznań", java.util.UUID.randomUUID().toString().substring(0, 2));
        ClientInformation clientInformation = new ClientInformation("Pawel Szudrowicz", "111111111");

        this.user = bank.createClient(clientInformation, new LinearInterestCalculator());
    }


}
