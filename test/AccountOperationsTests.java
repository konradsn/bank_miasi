import com.bank.accounts.Account;
import com.bank.accounts.Debit;
import com.bank.client.Bank;
import com.bank.client.Client;
import com.bank.client.ClientInformation;
import com.bank.interest.LinearInterestCalculator;
import com.bank.operations.Mediator;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class AccountOperationsTests {

    private List<Client> users = new ArrayList<>();
    private List<Bank> banks = new ArrayList<>();
    private Mediator mediator = new Mediator();

    @Before
    public void setUp() {
        Bank bank = new Bank("MYBANK", "ul. Bankowa 1, Poznań", generateUniqueBankId());
        Bank bank2 = new Bank("MONEYBANK", "ul. Wroniecka 1, Poznań", generateUniqueBankId());

        banks.add(bank);
        banks.add(bank2);

        mediator.addBankToCIR(bank);
        mediator.addBankToCIR(bank2);

        ClientInformation clientInformation = new ClientInformation("Pawel Szudrowicz", "111111111");
        ClientInformation clientInformation2 = new ClientInformation("Konrad Sniatala", "111111112");

        users.add(bank.createClient(clientInformation, new LinearInterestCalculator()));
        users.add(bank2.createClient(clientInformation2, new LinearInterestCalculator()));
    }


    private String generateUniqueBankId() {
        String id = java.util.UUID.randomUUID().toString().substring(0, 2);
        if (banks.stream().anyMatch(bank -> bank.getBankId().equals(id))) {
            generateUniqueBankId();
        }
        return id;
    }

    @Test
    public void makeDeposit() {
        //Given
        Client user = users.get(0);
        Account userAccount = user.getAccounts().get(0);
        BigDecimal initialAmount = userAccount.getAmount();

        //When
        user.makeDeposit(userAccount, new BigDecimal(100));

        //Then
        Assert.assertEquals(initialAmount.add(new BigDecimal(100)), userAccount.getAmount());
    }

    @Test
    public void makeWithdraw() {
        //Given
        Client user = users.get(0);
        Account userAccount = user.getAccounts().get(0);
        BigDecimal initialAmount = userAccount.getAmount();

        //When
        user.makeWithdraw(userAccount, new BigDecimal(100));

        //Then
        Assert.assertEquals(initialAmount.subtract(new BigDecimal(100)), userAccount.getAmount());
    }

    //na koncie 1000
    //debit 0 (jest ustawiany w kontruktorze na 0),
    //wypłacam 1050,
    //kończę z na koncie 0,
    //debit 50
    @Test
    public void makeWithdrawWithDebit() {
        //Given
        Client user = users.get(0);
        Account userAccount = user.getAccounts().get(0);
        Debit userDebitAccount = new Debit(userAccount, user.getClientBank().getBankId(), new BigDecimal(100));

        //When
        user.makeDeposit(userDebitAccount, new BigDecimal(1000));
        user.makeWithdraw(userDebitAccount, new BigDecimal(1050));

        //Then
        Assert.assertEquals(new BigDecimal(0), userDebitAccount.getAmount());
        Assert.assertEquals(new BigDecimal(50), userDebitAccount.getDebit());

    }

    //na koncie 1000,
    //debit 0 (jest ustawiany w kontruktorze na 0)
    //wypłacam 1050
    //limit_debit 30,
    //to nie przejdzie, więc na koncie powinno zostać tyle ile było
    @Test
    public void makeWithdrawWithDebit2() {
        //Given
        Client user = users.get(0);
        Account userAccount = user.getAccounts().get(0);
        Debit userDebitAccount = new Debit(userAccount, user.getClientBank().getBankId(), new BigDecimal(30));

        //When
        user.makeDeposit(userDebitAccount, new BigDecimal(1000));
        user.makeWithdraw(userDebitAccount, new BigDecimal(1050));

        //Then
        Assert.assertEquals(new BigDecimal(1000), userDebitAccount.getAmount());
        Assert.assertEquals(new BigDecimal(0), userDebitAccount.getDebit());

    }

    //na koncie 1000,
    //debit 0 (jest ustawiany w kontruktorze na 0)
    //wypłacam 900
    //limit_debit 30,
    //ok spoko, zostało mi na koncie 900, debit 0
    @Test
    public void makeWithdrawWithDebit3() {
        //Given
        Client user = users.get(0);
        Account userAccount = user.getAccounts().get(0);
        Debit userDebitAccount = new Debit(userAccount, user.getClientBank().getBankId(), new BigDecimal(30));

        //When
        user.makeDeposit(userDebitAccount, new BigDecimal(1000));
        user.makeWithdraw(userDebitAccount, new BigDecimal(900));

        //Then
        Assert.assertEquals(new BigDecimal(100), userDebitAccount.getAmount());
        Assert.assertEquals(new BigDecimal(0), userDebitAccount.getDebit());
    }

    //na koncie 1000,
    //debit 0 (jest ustawiany w kontruktorze na 0)
    //wypłacam 1050
    //limit_debit 100,
    //na koncie 0, debit 50
    //po czym wpłacam 20, czyli na koniec na koncie nadal 0, a debit 30
    @Test
    public void makeWithdrawWithDebitAndThenDeposit() {
        //Given
        Client user = users.get(0);
        Account userAccount = user.getAccounts().get(0);
        Debit userDebitAccount = new Debit(userAccount, user.getClientBank().getBankId(), new BigDecimal(100));

        //When
        user.makeDeposit(userDebitAccount, new BigDecimal(1000));
        user.makeWithdraw(userDebitAccount, new BigDecimal(1050));

        //Then
        Assert.assertEquals(new BigDecimal(0), userDebitAccount.getAmount());
        Assert.assertEquals(new BigDecimal(50), userDebitAccount.getDebit());

        user.makeDeposit(userDebitAccount, new BigDecimal(20));

        Assert.assertEquals(new BigDecimal(0), userDebitAccount.getAmount());
        Assert.assertEquals(new BigDecimal(30), userDebitAccount.getDebit());
    }

    @Test
    public void makeInternalTransfer() {
        //Given
        Client user = users.get(0);
        Account userAccount = user.getAccounts().get(0);
        BigDecimal initialAmount = userAccount.getAmount();


        Account userAccount2 = user.createNewAccount(new LinearInterestCalculator());
        BigDecimal initialAmount2 = userAccount2.getAmount();

        //When
        try {
            user.makeAmountTransfer(userAccount, userAccount2.getAccountNumber(), new BigDecimal(100), mediator);
        } catch (Exception e) {
            e.printStackTrace();
        }

        //Then
        Assert.assertEquals(initialAmount.subtract(new BigDecimal(100)), userAccount.getAmount());
        Assert.assertEquals(initialAmount2.add(new BigDecimal(100)), userAccount2.getAmount());
    }

    @Test
    public void makeExternalTransferWhenAccountExist() {
        //Given
        Client user = users.get(0);
        Client user2 = users.get(1);

        Account userAccount = user.getAccounts().get(0);
        Account userAccount2 = user2.getAccounts().get(0);
        String targetAccountNumber = userAccount2.getAccountNumber();

        BigDecimal initialAmount = userAccount.getAmount();
        BigDecimal initialAmount2 = userAccount2.getAmount();

        //When
        try {
            user.makeAmountTransfer(userAccount, targetAccountNumber, new BigDecimal(100), mediator);
        } catch (Exception e) {
            e.printStackTrace();
        }

        //Then
        Assert.assertEquals(initialAmount.subtract(new BigDecimal(100)), userAccount.getAmount());
        Assert.assertEquals(initialAmount2.add(new BigDecimal(100)), userAccount2.getAmount());
    }

    @Test
    public void throwErrorWhenExternalAccountDoesntExist() {
        //Given
        Client user = users.get(0);

        Account userAccount = user.getAccounts().get(0);
        String targetAccountNumber = "1641fsdfbhj1234fbshfbf123";

        //When
        try {
            user.makeAmountTransfer(userAccount, targetAccountNumber, new BigDecimal(100), mediator);
        } catch (Exception e) {
            //Then
            Assert.assertEquals("Destination account doesn't exist!", e.getMessage());
        }
    }
}
