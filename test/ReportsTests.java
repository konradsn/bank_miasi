import com.bank.accounts.Account;
import com.bank.accounts.Credit;
import com.bank.client.Bank;
import com.bank.client.Client;
import com.bank.client.ClientInformation;
import com.bank.interest.LinearInterestCalculator;
import com.bank.reports.AllReport;
import com.bank.reports.Over500Report;
import org.junit.Assert;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.List;

public class ReportsTests {

    private Bank bank;

    public void setUp() {
        this.bank = new Bank("MYBANK", "ul. Bankowa 1, Poznań", java.util.UUID.randomUUID().toString().substring(0, 2));
    }

    @Test
    public void makeOver500ReportPartOfAccounts() {
        setUp();

        //Given
        Client user = createClient();
        Account accountOver500 = user.createNewAccount(new LinearInterestCalculator());
        Account emptyAccount = user.createNewAccount(new LinearInterestCalculator());

        user.makeDeposit(accountOver500, new BigDecimal(600));

        //When
        List<Account> over500ReportAccountList = bank.makeReport(new Over500Report());

        //Then
        Assert.assertEquals(1, over500ReportAccountList.size());
    }

    @Test
    public void makeOver500ReportAllAccounts() {
        setUp();

        //Given
        Client user = createClient();
        Account accountOver500 = user.createNewAccount(new LinearInterestCalculator());
        Account accountOver600 = user.createNewAccount(new LinearInterestCalculator());

        user.makeDeposit(accountOver500, new BigDecimal(550));
        user.makeDeposit(accountOver600, new BigDecimal(650));

        //When
        List<Account> over500ReportAccountList = bank.makeReport(new Over500Report());

        //Then
        Assert.assertEquals(2, over500ReportAccountList.size());
    }

    @Test
    public void makeAllReport() {
        //Given
        setUp();
        Client user = createClient();
        Account account1 = user.createNewAccount(new LinearInterestCalculator());
        Account account2 = user.createNewAccount(new LinearInterestCalculator());
        Account account3 = user.createNewAccount(new LinearInterestCalculator());

        user.makeDeposit(account1, new BigDecimal(600));
        user.makeDeposit(account2, new BigDecimal(200));

        //When
        List<Account> over500ReportAccountList = bank.makeReport(new AllReport());

        //Then
        Assert.assertEquals(4, over500ReportAccountList.size());
    }

    @Test
    public void makeAllReportWithCredits() {
        //Given
        setUp();
        Client user = createClient();
        Account account1 = user.createNewAccount(new LinearInterestCalculator());
        Account account2 = user.createNewAccount(new LinearInterestCalculator());
        Credit userCredit = new Credit(account2, user.getClientBank().getBankId());
        user.addAccount(userCredit);

        user.makeDeposit(account1, new BigDecimal(600));
        user.makeDeposit(account2, new BigDecimal(200));
        user.makeDeposit(userCredit, new BigDecimal(200));


        //When
        List<Account> allReportAccountList = bank.makeReport(new AllReport());

        //Then
        Assert.assertEquals(4, allReportAccountList.size());
        System.out.println(allReportAccountList);
    }

    private Client createClient() {
        ClientInformation clientInformation = new ClientInformation("Pawel Szudrowicz", "111111111");

        return bank.createClient(clientInformation, new LinearInterestCalculator());
    }


}
