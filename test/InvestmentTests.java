import com.bank.accounts.Account;
import com.bank.accounts.Debit;
import com.bank.accounts.Investment;
import com.bank.client.Bank;
import com.bank.client.Client;
import com.bank.client.ClientInformation;
import com.bank.interest.LinearInterestCalculator;
import com.bank.operations.Mediator;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class InvestmentTests {

    private List<Client> users = new ArrayList<>();
    private List<Bank> banks = new ArrayList<>();

    @Before
    public void setUp() {
        Bank bank = new Bank("MYBANK", "ul. Bankowa 1, Poznań", generateUniqueBankId());

        banks.add(bank);

        ClientInformation clientInformation = new ClientInformation("Filip Philavong", "111111111");

        users.add(bank.createClient(clientInformation, new LinearInterestCalculator(0.2f)));
    }

    private String generateUniqueBankId() {
        String id = java.util.UUID.randomUUID().toString().substring(0, 2);
        if (banks.stream().anyMatch(bank -> bank.getBankId().equals(id))) {
            generateUniqueBankId();
        }
        return id;
    }

    @Test
    public void createInvestmentWith100zł() {
        //Given
        Client user = users.get(0);
        Account userAccount = user.getAccounts().get(0);
        user.makeDeposit(userAccount, new BigDecimal(1000));

        BigDecimal initialAmount = userAccount.getAmount();
        Investment userInvestment = new Investment(userAccount, user.getClientBank().getBankId());
        user.addAccount(userInvestment);

        //When
        user.makeDeposit(userInvestment, new BigDecimal(100));

        //Then
        Assert.assertEquals(initialAmount.subtract(new BigDecimal(100)), userAccount.getAmount());
        Assert.assertEquals(new BigDecimal(100), userInvestment.getInvestmentAmount());
    }

    @Test
    public void closeInvestmentBeforeTime() {
        //Given
        Client user = users.get(0);
        Account userAccount = user.getAccounts().get(0);
        user.makeDeposit(userAccount, new BigDecimal(1000));

        BigDecimal initialAmount = userAccount.getAmount();
        Investment userInvestment = new Investment(userAccount, user.getClientBank().getBankId());
        user.addAccount(userInvestment);

        //When
        user.makeDeposit(userInvestment, new BigDecimal(100));
        user.closeInvestmentBeforeTime(userInvestment);

        //Then
        Assert.assertEquals(initialAmount, userAccount.getAmount());
    }

    @Test
    public void closeInvestmentOnTime() {
        //Given
        Client user = users.get(0);
        Account userAccount = user.getAccounts().get(0);
        user.makeDeposit(userAccount, new BigDecimal(1000));
        Investment userInvestment = new Investment(userAccount, user.getClientBank().getBankId());
        user.addAccount(userInvestment);

        //When
        user.makeDeposit(userInvestment, new BigDecimal(100));
        user.closeInvestmentOnTime(userInvestment);

        BigDecimal truncatedAmount= userAccount.getAmount().setScale(0,BigDecimal.ROUND_DOWN);
        //Then
        Assert.assertEquals(new BigDecimal(1020), truncatedAmount);

    }
}
