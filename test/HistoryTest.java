import com.bank.accounts.Account;
import com.bank.client.Bank;
import com.bank.client.Client;
import com.bank.client.ClientInformation;
import com.bank.interest.LinearInterestCalculator;
import com.bank.listeners.AccountOperationHistoryListener;
import org.junit.Assert;
import org.junit.Test;

import java.math.BigDecimal;

public class HistoryTest {
    private Client user;

    public void setUp() {
        Bank bank = new Bank("MYBANK", "ul. Bankowa 1, Poznań", java.util.UUID.randomUUID().toString().substring(0, 2));
        ClientInformation clientInformation = new ClientInformation("Pawel Szudrowicz", "111111111");

        this.user = bank.createClient(clientInformation, new LinearInterestCalculator());
    }

    @Test
    public void testHistoryLogging() {
        setUp();

        Account account = user.getAccounts().get(0);

        user.makeDeposit(account, new BigDecimal(10));

        Assert.assertEquals(0, account.getAccountHistory().getAccountOperationHistoryList().size());

        account.addAccountOperationListener(new AccountOperationHistoryListener());

        user.makeDeposit(account, new BigDecimal(10));

        Assert.assertEquals(1, account.getAccountHistory().getAccountOperationHistoryList().size());
    }
}
